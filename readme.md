# learnopengl

* https://learnopengl-cn.github.io/


```bash
sudo apt-get install libglfw3 libglfw3-dev
sudo apt-get install cmake libglu1-mesa-dev mesa-common-dev
sudo apt-get install libgl1-mesa-dev
sudo apt-get install libgl-dev
sudo apt-get install libglm-dev
```


## Download  Glad

* [GLAD在线服务](https://glad.dav1d.de/)
* [配置GLAD](https://learnopengl-cn.github.io/01%20Getting%20started/02%20Creating%20a%20window/#glad)
	* Language: C/C++
	* Specification : OpenGL
	* gl : Version 3.3
	* Core : Core
	* GENERATE

## 5.1

```bash
sudo apt-get install libglm-dev
```
```cpp

glm::mat4 model         = glm::mat4(1.0f); 
model = glm::rotate(model, glm::radians(-55.0f), glm::vec3(1.0f, 0.0f, 0.0f)); 
```
根据你提供的代码，首先创建了一个单位矩阵\(\text{model}\)然后对其进行了绕X轴旋转的操作。在这种情况下，要表示绕X轴逆时针旋转角度\(-55.0\)度的变换矩阵，可以使用以下线性代数矩阵表示：

假设初始的单位矩阵\(\text{model}\)为：

\[ \text{model} = \begin{bmatrix} 1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \\ 0 & 0 & 1 & 0 \\ 0 & 0 & 0 & 1 \end{bmatrix} \]

绕X轴逆时针旋转角度\(\theta\)的旋转矩阵表示为：

\[ R_x = \begin{bmatrix} 1 & 0 & 0 & 0 \\ 0 & \cos(\theta) & -\sin(\theta) & 0 \\ 0 & \sin(\theta) & \cos(\theta) & 0 \\ 0 & 0 & 0 & 1 \end{bmatrix} \]

其中\(\theta = -55.0\)度。将单位矩阵\(\text{model}\)与旋转矩阵\(R_x\)相乘得到旋转后的模型矩阵\(\text{model}_{\text{rotated}}\)：

\[ \text{model}_{\text{rotated}} = R_x \times \text{model} \]

按照矩阵乘法规则进行计算，即可得到旋转后的模型矩阵\(\text{model}_{\text{rotated}}\)。

----------------


```cpp
glm::mat4 view;
// 注意，我们将矩阵向我们要进行移动场景的反方向移动。
view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));
```

这段代码使用了OpenGL数学库（glm）来创建一个视图矩阵，并将其沿着Z轴的负方向平移3个单位。使用数学矩阵表示的话，可以写成如下形式：

假设初始的view矩阵为I（即单位矩阵），平移矩阵T为：

\[ T = \begin{bmatrix} 1 & 0 & 0 & 0 \\ 0 & 1 & 0 & 0 \\ 0 & 0 & 1 & -3 \\ 0 & 0 & 0 & 1 \end{bmatrix} \]

则执行 `view = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));` 后，得到的新的view矩阵为：

\[ view = T \times I = T \]

即新的view矩阵就是平移矩阵T。


```cpp
glm::mat4 projection;
projection = glm::perspective(glm::radians(45.0f), screenWidth / screenHeight, 0.1f, 100.0f);
```


这段代码使用了OpenGL数学库（glm）来创建透视投影矩阵。透视投影矩阵可以用来将3D场景投影到2D屏幕上，以便进行渲染。

透视投影矩阵的数学表示如下：

假设透视投影矩阵为P，则根据给定的参数，它可以表示为：

\[ P = \begin{bmatrix} \frac{1}{\text{aspectRatio} \times \tan(\frac{\text{fov}}{2})} & 0 & 0 & 0 \\ 0 & \frac{1}{\tan(\frac{\text{fov}}{2})} & 0 & 0 \\ 0 & 0 & -\frac{\text{far}+\text{near}}{\text{far}-\text{near}} & -\frac{2 \times \text{far} \times \text{near}}{\text{far}-\text{near}} \\ 0 & 0 & -1 & 0 \end{bmatrix} \]

其中，\(\text{fov}\) 是视野角度（单位为弧度），\(\text{aspectRatio}\) 是屏幕宽高比，\(\text{near}\) 和 \(\text{far}\) 分别是近裁剪面和远裁剪面的距离。




## 7.1

```cpp
glm::mat4 view = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
float radius = 10.0f;
float camX = static_cast<float>(sin(glfwGetTime()) * radius);
float camZ = static_cast<float>(cos(glfwGetTime()) * radius);
view = glm::lookAt(glm::vec3(camX, 0.0f, camZ), \
				   glm::vec3(0.0f, 0.0f, 0.0f), \
				   glm::vec3(0.0f, 1.0f, 0.0f));
```

根据你提供的代码，首先创建了一个单位矩阵\(\text{view}\)，然后使用glm库中的函数\(\text{glm::lookAt}\)来设置观察矩阵。这个函数用于创建一个观察矩阵，以模拟摄像机从一个位置观察目标的场景。

对于上述代码中的观察矩阵的计算过程，可以使用以下方式来表示：

假设摄像机位置为\((camX, 0.0, camZ)\)，观察目标位置为\((0.0, 0.0, 0.0)\)，摄像机向上的方向为\((0.0, 1.0, 0.0)\)。那么观察矩阵\(\text{view}\)可以通过\(\text{glm::lookAt}\)函数得到。

观察矩阵\(\text{view}\)表示为：

\[ \text{view} = \begin{bmatrix} right.x & up.x & -forward.x & 0 \\ right.y & up.y & -forward.y & 0 \\ right.z & up.z & -forward.z & 0 \\ -dot(right, eye) & -dot(up, eye) & dot(forward, eye) & 1 \end{bmatrix} \]

其中\(right\)、\(up\)、\(forward\)分别代表摄像机坐标系的三个轴向量，\(eye\)代表摄像机位置。这些值会被\(\text{glm::lookAt}\)函数计算得出。