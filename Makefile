# 编译器设置
CXX = g++
CXXFLAGS = -Wall -g

# 包含路径
INCLUDES = -I./include `pkg-config --cflags glfw3`

# 链接库
LDLIBS = `pkg-config --static --libs glfw3` -lGL -lGLU -lglut -ldl

# 源文件和目标文件
SOURCES = hello_window_clear.cpp   src/glad.c
OBJECTS = $(SOURCES:.cpp=.o)
TARGET = hello_window_clear  

# 默认目标
all: $(TARGET)

# 链接目标
$(TARGET): $(OBJECTS)
	$(CXX) $(LDFLAGS) $(INCLUDES) -o $@ $^ $(LDLIBS)

# 编译规则
%.o: %.cpp
	$(CXX) $(CXXFLAGS) $(INCLUDES) -c $<

# 清理目标
clean:
	rm -f $(OBJECTS) $(TARGET)

.PHONY: all clean
