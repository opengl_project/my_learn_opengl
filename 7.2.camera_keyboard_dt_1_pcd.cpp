#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <learnopengl/shader_m.h>

//
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <iostream>

#include <omp.h>

int numberOfCores = 4; // 线程数量

void convertPCLPointCloudToVertices(const pcl::PointCloud<pcl::PointXYZRGB>& cloud, std::vector<float>& vertices)
{
    int numberOfCores = 4; // 线程数量
    #pragma omp parallel for num_threads(numberOfCores)
    for (const auto& point : cloud.points)
    {
        // 使用原子操作将结果存储在 vertices 中
        #pragma omp critical
        {
            vertices.push_back(point.x);
            vertices.push_back(point.y);
            vertices.push_back(point.z);

            // Normalize RGB values to range [0, 1] and store in vertices
            vertices.push_back(1.0f);
            vertices.push_back(0.0f);
            vertices.push_back(0.0f);
        }

        // vertices.push_back(point.r / 255.0f);
        // vertices.push_back(point.g / 255.0f);
        // vertices.push_back(point.b / 255.0f);
    }
}

void normalizePointCloud(std::vector<float>& vertices)
{
    // Find min and max values in x, y, z dimensions
    float minX = std::numeric_limits<float>::max();
    float maxX = -std::numeric_limits<float>::max();
    float minY = std::numeric_limits<float>::max();
    float maxY = -std::numeric_limits<float>::max();
    float minZ = std::numeric_limits<float>::max();
    float maxZ = -std::numeric_limits<float>::max();

     #pragma omp parallel for num_threads(numberOfCores)
    for (size_t i = 0; i < vertices.size(); i += 6) // Assuming each vertex contains XYZRGB data
    {
        minX = std::min(minX, vertices[i]);
        maxX = std::max(maxX, vertices[i]);
        minY = std::min(minY, vertices[i + 1]);
        maxY = std::max(maxY, vertices[i + 1]);
        minZ = std::min(minZ, vertices[i + 2]);
        maxZ = std::max(maxZ, vertices[i + 2]);
    }
    // Calculate scale factors for x, y, z dimensions
    float scaleX = 2.0f / (maxX - minX);
    float scaleY = 2.0f / (maxY - minY);
    float scaleZ = 2.0f / (maxZ - minZ);
    std::cout << "scaleX : "<< scaleX << std::endl;
    std::cout << "scaleY : "<< scaleY << std::endl;
    std::cout << "scaleZ : "<< scaleZ << std::endl;

    // Calculate translate factors for x, y, z dimensions
    float translateX = -0.5f * (maxX + minX) * scaleX;
    float translateY = -0.5f * (maxY + minY) * scaleY;
    float translateZ = -0.5f * (maxZ + minZ) * scaleZ;

    // Normalize vertices
    #pragma omp parallel for num_threads(numberOfCores)
    for (size_t i = 0; i < vertices.size(); i += 6)
    {
        float h = vertices[i + 2];
        if (h > 20) h = 20;
        if (h < -5) h = -5;

        
        vertices[i] = (vertices[i] * scaleX + translateX) ;
        vertices[i + 1] = (vertices[i + 1] * scaleY + translateY);
        vertices[i + 2] = (vertices[i + 2] * scaleZ + translateZ);

        // corlor
        vertices[i + 3] = (h + 15) / 35.0;
        vertices[i + 4] = (h + 15) / 35.0;
        vertices[i + 5] = (h + 15) / 35.0;
    }
}



void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// camera
glm::vec3 cameraPos   = glm::vec3(0.0f, 0.0f,  1.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f,  -0.1f);
glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f,  0.0f);


// timing
float deltaTime = 0.0f;	// time between current frame and last frame
float lastFrame = 0.0f;


Shader * ourShader_ptr = nullptr;
float distance_k = 1.0;;

int main()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    // glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    // configure global opengl state
    // -----------------------------
    glEnable(GL_DEPTH_TEST);

    // 设置视口和注册窗口大小回调函数
    int screenWidth, screenHeight;
    glfwGetFramebufferSize(window, &screenWidth, &screenHeight);
    glViewport(0, 0, screenWidth, screenHeight);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    // build and compile our shader zprogram
    // ------------------------------------
    Shader ourShader("7.2.camera_pcd.vs", "7.2.camera_pcd.fs");

    // char * pcd_path = "/home/promote/autoware.docker/autoware.2023.11/maps/sample-map-planning/pointcloud_map.pcd";
    const char * pcd_path = "/home/promote/autoware.docker/autoware.2023.11/maps/jigang_map3/pointcloud_map.pcd";

    // 读取PCD文件
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
    // pcl::io::loadPCDFile("point_cloud.pcd", *cloud);

    pcl::io::loadPCDFile(pcd_path,*cloud);
    std::cout << "point_cloud : " << cloud->size() << std::endl;
    std::cout << " size " <<sizeof(cloud->points[0]) << std::endl;

    std::vector<float>  vertices_pcd;
    convertPCLPointCloudToVertices(*cloud,vertices_pcd);
    normalizePointCloud(vertices_pcd);

    // set up vertex data (and buffer(s)) and configure vertex attributes
    // ------------------------------------------------------------------
    // world space positions of our cubes
    glm::vec3 cubePositions[] = {
        glm::vec3( 0.0f,  0.0f,  0.0f),
    };
    unsigned int VBO, VAO;
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);

    glBindVertexArray(VAO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, vertices_pcd.size() * sizeof(float), vertices_pcd.data(), GL_STATIC_DRAW);

    // position attribute
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // color attribute
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    ourShader.use();
    glm::mat4 projection = glm::ortho(-1.0f, 1.0f, -1.0f, 1.0f, 0.1f, 100.0f); // 正射投影矩阵
    ourShader.setMat4("projection", projection);
    ourShader_ptr = &ourShader;

    // render loop
    // -----------
    while (!glfwWindowShouldClose(window))
    {
        // per-frame time logic
        // --------------------
        float currentFrame = static_cast<float>(glfwGetTime());
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // input
        // -----
        processInput(window);

        // render
        // ------
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 

        // activate shader
        ourShader.use();

        // camera/view transformation
        glm::mat4 view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
        // glm::mat4 view = glm::lookAt( glm::vec3(0.0f, 0.0f, 1.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
        ourShader.setMat4("view", view);

        // render boxes
        glBindVertexArray(VAO);
        // calculate the model matrix for each object and pass it to shader before drawing
        glm::mat4 model = glm::mat4(1.0f); // make sure to initialize matrix to identity matrix first
        model = glm::translate(model, cubePositions[0]);
        float angle = 0.0f;
        model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.0f, 0.0f));
        ourShader.setMat4("model", model);

        // glPointSize(0.1f); // 这里设置点的大小
        glDrawArrays(GL_POINTS, 0, vertices_pcd.size() / 6); // 每个顶点包含 XYZRGB 共 6 个值

        // glfw: swap buffers and poll IO events (keys pressed/released, mouse moved etc.)
        // -------------------------------------------------------------------------------
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    // optional: de-allocate all resources once they've outlived their purpose:
    // ------------------------------------------------------------------------
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);

    // glfw: terminate, clearing all previously allocated GLFW resources.
    // ------------------------------------------------------------------
    glfwTerminate();
    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    float cameraSpeed = static_cast<float>(2.5 * deltaTime);
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        // cameraPos += cameraSpeed * cameraFront;
        cameraPos += cameraSpeed * cameraUp;
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        // cameraPos -= cameraSpeed * cameraFront;
        cameraPos -= cameraSpeed * cameraUp;
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS){
        if (distance_k<2.0){
            distance_k += cameraSpeed * 0.5;
        }
        glm::mat4 projection = glm::ortho(-1.0f * distance_k, 1.0f* distance_k, -1.0f * distance_k, 1.0f * distance_k, 0.1f, 100.0f); // 正射投影矩阵
        if (ourShader_ptr != nullptr){
            ourShader_ptr->setMat4("projection", projection);
        } 
    }
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS){
        if (distance_k>0.1){
            distance_k -= cameraSpeed * 0.5;
        }
        glm::mat4 projection = glm::ortho(-1.0f * distance_k, 1.0f* distance_k, -1.0f * distance_k, 1.0f * distance_k, 0.1f, 100.0f); // 正射投影矩阵
        if (ourShader_ptr != nullptr){
            ourShader_ptr->setMat4("projection", projection);
        }
    }

}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);

    // // 更新正射投影矩阵
    // float aspectRatio = static_cast<float>(width) / static_cast<float>(height);
    // glm::mat4 projection = glm::ortho(-10.0f * aspectRatio, 10.0f * aspectRatio, -10.0f, 10.0f, 0.1f, 100.0f);
    // ourShader.setMat4("projection", projection);
}
